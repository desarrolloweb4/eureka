FROM openjdk:8-jdk-alpine
COPY "./target/eureka-0.0.1-SNAPSHOT.jar" "app.jar"
RUN apt-get update
RUN apt-get install nano net-tools vim
EXPOSE 2323
ENTRYPOINT [ "java", "-jar", "app.jar" ]